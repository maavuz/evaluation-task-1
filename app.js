const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// Data file to keep the code DRY
const data = require('./data.json');

app.get('/', function(req, res) {
  res.sendFile(__dirname + './index.html');
});

/**
 * Since we dont have the actual data here,
 * use this function to generate random data and send it to the connected clients
 *
 * @param {Array} arr Pass data array
 * @returns {Object} random data object from given array
 */
function getRandomData(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

// You can give a Socket.io server arbitrary functions via io.use() that are run when a socket is created.
// we are using it to authenticate connection
io.use(function(socket, next) {
  // Get handshake data
  const handshakeData = socket.handshake.query;
  const userName = handshakeData.username;
  const userPassword = handshakeData.password;

  // verify login credentials. this is only for demostration purposes. obviously we won't hard-code login credentials and verify these like this.
  // TODO: Use a JWT (https://www.npmjs.com/package/socketio-jwt) for authentication
  if (userName === 'demo' && userPassword === 'demo') {
    // Allo good. lets proceed next.
    next();
  } else {
    // Auth failed. send error
    next(
      new Error(
        "{ 'code': 401, 'message': 'Authentication Error, please try again'}"
      )
    );
  }
});

// When a new socket connection is established
io.on('connection', function(socket) {
  // Send auth feedback
  socket.emit('message', 'Authentication successful, welcome');

  // Send Random Data to all connected socket clients every 5 seconds
  setInterval(function() {
    socket.emit('message', getRandomData(data));
  }, 5000);
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
