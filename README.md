# Evaluation Task - 1

### Installation

```sh
$ git clone git@bitbucket.org:maavuz/evaluation-task-1.git
$ cd on-task-1
$ npm install
```

### Runing

```sh
$ npm run app.js
and Open localhost:3000 in browser
```

if you open this in one tab it means that 1 client is connected. if you open the same url in different tab it means that a new client connection is established. this is the basic version it will send data to all connected socket clients every 5 second. you will be able to see the messages on html page.

### Logins

Username: demo
password: demo
